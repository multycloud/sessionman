<?php

namespace MultyCloud\SessionMan;

class Session {

    private static $instance;
    private $cookie = '';
    private $username = '';
    private $email = '';
    private $dn = '';
    private $expiry = '';
    private $services = [];
    private $cPanelData = '';

    public static function getInstance(){
        if(!self::$instance){
            self::setup();
        }
        return self::$instance;
    }

    public function getUsername(){
        return $this->username;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getDn(){
        return $this->dn;
    }

    public function getExpiry(){
        return $this->expiry;
    }

    public function getCPanelData(){
        if(!$this->cPanelData) $this->readCPanelData();
        return $this->cPanelData;
    }

    public function getServices(){
        return $this->services;
    }

    public function get($var){
        return $this->{$var};
    }

    public function readCookieData($save = false){
        $filename = $this->getCookieFilename();
        $file_exists = file_exists($filename);
        $data = $file_exists ? \json_decode(\file_get_contents($filename)) : [];
        $this->setData($data, false);
    }

    public function readCPanelData(){
        $filename = $this->getCPanelFilename();
        $file_exists = file_exists($filename);
        $data = $file_exists ? \json_decode(\file_get_contents($filename)) : (object) [];
        $this->cPanelData = $data;
    }

    public function setData($data, $save = true){
        foreach($data as $key => $value){
            $this->{$key} = $value;
        }
        if($save) $this->save();
    }
    
    public function set($var, $value){
        return $this->{$var} = $value;
    }

    public function setCPanelData($data){
        \file_put_contents($this->getCPanelFilename(), \json_encode($data));
    }

    public static function setup(){
        self::$instance = new Session();
        self::$instance->cookie = self::getCookie();
        self::$instance->setCookie();
        self::$instance->readCookieData();
    }

    public static function read(){
        self::$instance = new Session();
        self::$instance->cookie = self::getCookie();
        self::$instance->readCookieData();
    }

    public static function logout(){
        if(isset($_COOKIE['multycloud'])){
            $cookie = $_COOKIE['multycloud'];
            unlink(self::getCookieFilename($cookie));
            self::destroyCookie();
        }
    }

    public function save(){
        \file_put_contents(self::getCookieFilename($this->cookie), \json_encode((object) array_merge(\get_object_vars($this), [
            'expiry' => $this->expiry
        ])));
    }

    private function setCookie(){
        $this->expiry = time() + (15 * 24 * 60 * 60);
        \setcookie('multycloud', $this->cookie, $this->expiry, '/', 'multy.cloud', TRUE);
    }

    private static function destroyCookie(){
        \setcookie('multycloud', '', time()-3600, '/', 'multy.cloud');
    }

    private static function getCookie(){
        global $_COOKIE;
        if(isset($_COOKIE['multycloud'])){
            return $_COOKIE['multycloud'];
        }
        else {
            return self::generateCookie();
        }
    }

    private static function generateCookie(){
        $cookie = uniqid();
        if(\file_exists(self::getCookieFilename($cookie))){
            return self::generateCookie();
        }
        return $cookie;
    }

    public static function getCookieFromFilename($file){
        return str_replace('/sessions/', '', str_replace('.json', '', $file));
    }

    public function getCookieFilename($cookie = ''){
        if ($cookie) {
            return '/sessions/' . $cookie . '.json';
        }
        else if(isset($this) && $this instanceof self){
            return '/sessions/' . $this->cookie . '.json';
        }
        throw new Exception("Trying to do the impossible.");
    }

    public function getCPanelFilename($email = ''){
        if ($email) {
            return '/sessions/cpanel/' . $email . '.json';
        }
        else if(isset($this) && $this instanceof self){
            return '/sessions/cpanel/' . $this->email . '.json';
        }
        throw new Exception("Trying to do the impossible.");
    }

    public static function getAllSessions(){
        $files = glob(self::getCookieFilename("*"));
        return array_map(function($file){
            $cookie = self::getCookieFromFilename($file);
            $session = new Session();
            $session->cookie = $cookie;
            $session->readCookieData();
            return $session;
        }, $files);
    }

    public static function destroyOldSessions($sessions){
        return array_filter($sessions, function($session){
            if($session->expiry >= time()){
                return true;
            }
            else {
                $session->destroy();
            }
        });
    }

    public function destroy(){
        unlink($this->getCookieFilename());
        return false;
    }

    public function setInstance($session){
        self::$instance = $session;
    }
}
